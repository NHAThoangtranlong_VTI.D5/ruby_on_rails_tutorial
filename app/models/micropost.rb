class Micropost < ApplicationRecord
  belongs_to :user
  has_one_attached :image

  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: Settings.micropost.content_max_length }
  validates :image, content_type: { in: Settings.micropost.ccontent_type_image,
                                    message: "must be a valid image format"},
                    size:         { less_than: Settings.micropost.image_size.megabytes,
                                    message: "should be less than 5MB"}
  scope :recent_posts, ->{order created_at: :desc}
  scope :micropost_feed, ->user_id, following_ids {where("user_id IN (:following_ids) OR user_id = :user_id",
                            following_ids: following_ids, user_id: user_id)}

  def display_image
    image.variant(resize_to_limit: [Settings.number.size, Settings.number.size])
  end
end
