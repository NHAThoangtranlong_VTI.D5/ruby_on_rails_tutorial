class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user&.authenticate params[:session][:password]
      if user.activated
        log_in user
        check_remember user
        flash[:success] = t "layouts.application.activate_success"
        redirect_back_or user
      else
        flash[:warning] = t "layouts.application.activate_fail"
        redirect_to root_url
      end
    else
      flash[:warning] = t "layouts.application.login_fail"
      redirect_to login_url
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end