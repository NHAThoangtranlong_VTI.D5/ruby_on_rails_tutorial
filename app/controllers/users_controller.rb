class UsersController < ApplicationController
  before_action :find_user, except: [:index, :new, :create]
  before_action :logged_in_user, except: [:new, :create]
  before_action :correct_user, only: [:edit, :update]

  def show
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params) 
    if @user.save
      @user.send_activation_email
      flash[:info] = t "layouts.application.check_email_to_activate"
      redirect_to root_url
    else
      flash[:danger] = t "layouts.application.inform_failed"
      render 'new'
    end
  end

  def update
    if @user.update(user_params)
      flash[:success] = t ("users.new.profile_updated")
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    if @user.destroy
      flash[:success] = t("users.new.user_deleted")
    else
      flash[:danger] = t(".users.new.delete_fail")
    end
    redirect_to users_url
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
  
    def find_user
      @user = User.find_by id: params[:id]
    end

    def logged_in_user
      unless logged_in?
      store_location
      flash[:danger] = t "layouts.application.please_log"
      redirect_to login_url
      end
    end

    def correct_user
      redirect_to(root_url) unless current_user?(@user)
    end
end
